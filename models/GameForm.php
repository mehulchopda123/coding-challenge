<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * ContactForm is the model behind the contact form.
 */
class GameForm extends Model
{
    public $choice;
    const ROCK = 'rock';
    const PAPER = 'paper';
    const SCISSOR = 'scissor';

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'choice' => '',
        ];
    }

    /**
     * Decide game result using the information collected by this model.
     * @param string $email the target email address
     * @return bool whether the model passes validation
     */
    public function playGame($humanchoice)
    {
            if ($humanchoice == self::PAPER) {
                $result = 'draw';
            } elseif ($humanchoice == self::ROCK) {
                $result = 'lose';
            } else {
                $result = 'win';
            }
            return $result;
    }
    /**
     * Decide user's choice using the information collected by this model.
     * @param string $email the target email address
     * @return bool whether the model passes validation
     */
    public function chooseItem()
    {
        $choices = ['rock', 'paper', 'scissor'];
        $choice = array_rand($choices);
        return $choices[$choice];

    }
}
