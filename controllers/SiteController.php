<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\GameForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }


    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Displays about Rock-Paper-Scissors.
     *
     * @return string
     */
    public function actionGame()
    {

        $model = new GameForm();
        //when the form submitted then handle the post request
        if ($model->load(Yii::$app->request->post())) {

            //randomize the choice of the user
            $model->choice = $model->chooseItem();
            //decide the result based on user's choice
            $result = $model->playGame($model->choice);
            //store the results in the session.
            if (Yii::$app->session[$result]) {
                Yii::$app->session[$result] = Yii::$app->session[$result] + 1;
            } else {
                Yii::$app->session[$result] = 1;
            }
            Yii::$app->session['current-result']=$result;
            Yii::$app->session->setFlash('gameFormSubmitted');
        }

        //check if the total game is 100 then destroy the session and shows the winner
        if (Yii::$app->session['win'] + Yii::$app->session['lose'] + Yii::$app->session['draw'] == 10) {
            if (Yii::$app->session['win'] > Yii::$app->session['lose']) {
                $winner = 'Human';
            } elseif (Yii::$app->session['win'] < Yii::$app->session['lose']) {
                $winner = 'Computer';
            } else {
                $winner = 'Draw';
            }
        }

        return $this->render('game', [
            'model' => $model,
            'winner' => isset($winner) ? $winner : '',
        ]);
    }
}
