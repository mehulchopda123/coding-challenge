<?php

namespace tests\unit\models;


class GameFormTest extends \Codeception\Test\Unit
{
    private $model;
    /**
     * @var \UnitTester
     */
    public $tester;

    public function testplayGame()
    {
        //define and validate model
        $this->model = $this->getMockBuilder('app\models\GameForm')
            ->setMethods(['validate'])
            ->getMock();
        //check whether expected result is : draw
        $this->tester->assertEquals('draw',$this->model->playGame('paper'));
        //check whether expected result is : lose
        $this->tester->assertEquals('lose',$this->model->playGame('rock'));
        //check whether expected result is : win
        $this->tester->assertEquals('win',$this->model->playGame('scissor'));
        //check whether random string function returns atleast one string
        $this->tester->assertNotEmpty($this->model->chooseItem());
    }
}
