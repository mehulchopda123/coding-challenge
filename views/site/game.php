<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */

/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Game';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="site-game">


    <?php if (Yii::$app->session->hasFlash('gameFormSubmitted')): ?>
        <?= Html::resetButton('Refresh', ['class' => 'btn btn-primary btn-black btn-lg btn-round', 'name' => 'game-button',
            'onClick' => 'window.location="' . Yii::$app->request->url . '"']) ?>
        <h1>Result : You <?php echo Yii::$app->session['current-result'] ?></h1>

    <?php else: ?>

        <?php $form = ActiveForm::begin(['id' => 'game-form']); ?>

        <?= $form->field($model, 'choice')->hiddenInput(['autofocus' => true]) ?>

        <div class="form-group">
            <?= Html::resetButton('Refresh', ['class' => 'btn btn-primary btn-black btn-lg btn-round', 'name' => 'game-button',
                'onClick' => 'window.location="' . Yii::$app->request->url . '"']) ?>
            <?= Html::submitButton('Play', ['class' => 'btn btn-primary btn-black btn-lg btn-round', 'name' => 'game-button']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    <?php endif; ?>
    <table class="table">
        <th>GAME WON</th>
        <th>GAME LOSE</th>
        <th>GAME DRAW</th>
        <tr>
            <td><?php echo Yii::$app->session['win']; ?></td>
            <td><?php echo Yii::$app->session['lose']; ?></td>
            <td><?php echo Yii::$app->session['draw']; ?></td>
        </tr>
    </table>

</div>
<!--destroy the session after the page load-->
<?php
if(!empty($winner)){
    echo $winner=='Draw'?'No Body Wins': $winner.' Wins';
    Yii::$app->session->destroy();
}
?>