<?php

/* @var $this yii\web\View */
use yii\helpers\Html;

$this->title = 'Gamigo Rock Paper Scissor';
?>
<div class="site-index">

    <div class="body-content">

        <div class="row">
           <div class="col-xs-12">
               <div class="jumbotron">
                  <ul>
                      <li><?= Html::a('Start Game', ['/site/game'], ['class'=>'btn btn-primary btn-black']) ?></li>
                      <li><?= Html::a('Contact', ['/site/contact'], ['class'=>'btn btn-primary btn-black']) ?></li>
                  </ul>

               </div>
           </div>
        </div>

    </div>
</div>
